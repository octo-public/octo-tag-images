# Injections de tags aux images
Ce projet permet d'ajouter des tags aux images proposées par la Créa d'OCTO.

Cela permet une recherche rapide et efficace sur Google Drive,
voir sur votre disque si vous avez synchronisé le répertoire `Graphic Tools`
et indexez le répartoire dans l'OS. 

Google Drive : `type:image octocréa risque`

![Search|width=100](.img/search_gdrive.png)

Ubuntu : `octocréa risque`

![Search|width=100](.img/search_ubuntu.png)

Windows : `octocréa risque sorte:picture` (Dans la [bibliothèque](http://www.aidewindows.net/seven/bibliotheques.php))

![Search|width=100](.img/search_windows.png)

MacOS : `octocréa risque`

![Search|width=100](.img/search_macos.png)

L'idée est d'utiliser l'intelligence collective d'OCTO pour améliorer les __#tags__
par rapport à vos besoins.

L'objectif est plutôt d'avoir trop de tags que moins. Donc, il ne faut pas
hésiter à en ajouter si vous souhaitez pouvoir retrouver les images
correspondantes à partir de votre mot clé.

## Participer
Pour y participer, il faut cloner ce projet 
```
git clone https://gitlab.com/octo-public/octo-tag-images.git
```
- demander un accès en écriture à PPR, puis récupérer une copie des images
de `Graphic Tools`. En effet, les droits ne sont pas ouverts sur ce répertoire,
pour éviter les maladresses.
- Ou faire un pull-request

Pour cela, le script `./bin/refresh` (`bin\refresh`sous Windows) permet d'indiquer où se
trouve les originaux de `Graphic Tools`. Il permet de recopier localement ces
images, pour pouvoir y travailler sans risque.

Vous pouvez alors apporter des modifications aux tags présents dans les images,
ou modifier le fichier `description.csv` qui reprend tous les tags des fichiers du
répertoire.

Lorsque vous souhaitez synchroniser cela, vous dever lancer la commande :

    bin/sync_tags

Cela a pour effet de réunir les tags présents dans le fichier CSV et dans les
images, et d'apporter les modifications nécessaires à une bonne indexation par
Google Drive.

Si vous êtes satisfait, le seul fichier qui compte est sont les fichiers
`description.csv` et `tags.txt`. Vous pouvez *committer* vos modifications.

Régulièrement, normalement deux fois par jours, une synchronisation est
appliquée sur les images du `Graphic Tools` original, 
via un compte ayant le droit de modifier les images. 
(_Mais pour le moment, envoyez moi un email que je puisse valider la procédure._)
Les tags décrit dans le fichier `descriptions.csv`sont alors injectés dans les images 
présentes dans Google Drive. Lors de la prochaine synchronisation, les tags deviennent 
disponibles pour vos recherches.

Consultez le fichier `description.csv` pour comprendre comment cela fonctionne.

Le principe est le suivant:
- Par défault, le programme récupère tous les tags présents dans les méta-datas des images,
les hashtags de la description et les tags de la base de données `description.csv`.
En cas de commentaire divergeant entre celui présent dans le fichier image et
celui présent dans la base de données, c'est ce dernier qui est prioritaire.
De par cette stratégie, il n'est pas possible de supprimer un tag, mais seulement en ajouter.
- En ajoutant le paramètre `--from-db`, seules les informations présentes dans le fichier `description.csv`
sont prises en compte. Les images sont ajustées en conséquence. Cela permet alors de supprimer
facilement des tags ou de corriger une faute sur un tag dans les images (enlever un pluriel par exemple)
- Enfin, en utilisant le paramètre `--from-files`, c'est l'inverse. Les images sont prioritaires et
vont modifier en conséquence le fichier `description.csv`. Cela est util si les corrections ont étés
pratiquées directement dans les images.

L'objectif est bien d'avoir le fichier `description.csv` à jour.

Si vous ne trouvez aucune image correspondant au tag que vous recherchez,
indiquez le dans ce [document](https://docs.google.com/spreadsheets/d/1p-FUZsSAiaL_RP7nA2p46t_Hpg0KTkd9fX24qh1kbrQ/edit?usp=sharing).
L'équipe Créa se chargera d'y remédier.

Pour modifier directement les images, l'outil [XnView](https://www.xnview.com/) est un bon
candidat. Il permet de gérer les tags des images. Pour une injection des tags
dans les images, il faut utiliser les paramètres suivants :

![XnView|width=100](.img/parametre_xnview.png)

Il est également possible d'utiliser [Adobe Bridge](https://helpx.adobe.com/fr/bridge/user-guide.html).


Les images ainsi annotées ne sont pas directement exploitables avec Google Drive. Il faut faire un peu le ménage.
C'est ce que propose l'outil [`tag_images_for_google_drive`](https://github.com/pprados/tag_images_for_google_drive)
que nous avons publié. La dernière version est
[ici](https://github.com/pprados/tag_images_for_google_drive/releases/latest).
Il permet d'injecter les tags dans les descriptions
des images et de maintenir en parallèle le fichier `description.csv`.
Pour plus d'information, consultez la documentation de l'outil.

PS: Pour modifier la localisation du répertoire d'origine `Graphic Tools`, 
effacez le fichier `.link` avant de relancer `bin/refresh`.

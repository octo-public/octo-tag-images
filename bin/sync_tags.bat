@chcp 65001 >nul:
@echo off
if not exist bin/tag_images_for_google_drive.exe (
    echo Download tag_images_for_google_drive...
    curl -L --silent "https://github.com/pprados/tag_images_for_google_drive/releases/download/v1.0/tag_images_for_google_drive.exe" ^
        --output bin/tag_images_for_google_drive.exe
    echo Download exiftool...
    curl -L --silent "https://github.com/pprados/tag_images_for_google_drive/releases/download/v1.0/exiftool.exe" ^
        --output bin/exiftool.exe
)

PATH=bin;%PATH%
bin\tag_images_for_google_drive %* -v --db descriptions.csv --tagfile tags.txt -t octocréa **/*.png **/*.jpg
bin\tag_images_for_google_drive %* -v --db descriptions.csv --tagfile tags.txt -t octocréa -t photo "Photos Licence OCTO/*.jpg"
echo Now "description.csv" and images are synchronized. You can publish your modifications in a commit

@chcp 65001 >nul:
@echo off

if exist .link (
	set /p src=<.link
) else (
	echo Vous n'avez pas les droits de modifiers les images de 'Graphic tools'.
	echo Il faut alors modifier une copie du répertoire.
	set /p src="Où se trouve votre répertoire synchronisé 'Graphic tools' :"
	rem Trim src
	setlocal enabledelayedexpansion
	for /f "tokens=* delims= " %%a in ("%src%") do set src=%%a
	for /l %%a in (1,1,100) do if "!src:~-1!"==" " set src=!src:~0,-1!

)
mkdir Avatars 2> NUL
mkdir Illustrations 2> NUL
mkdir Logos 2> NUL
mkdir "Photos Licence OCTO" 2> NUL
mkdir Pictogrammes 2> NUL
mkdir Illustrations 2> NUL
mkdir Illustrations 2> NUL

xcopy /s /y /d /q "%src%\Avatars" Avatars >nul:
xcopy /s /y /d /q "%src%\Illustrations" Illustrations >nul:
xcopy /s /y /d /q "%src%\Logos" Logos >nul:
xcopy /s /y /d /q "%src%\Photos Licence OCTO" "Photos Licence OCTO" >nul:
xcopy /s /y /d /q "%src%\Pictogrammes" Pictogrammes >nul:
xcopy /s /y /d /q "%src%\descriptions.csv" . >nul:
xcopy /s /y /d /q "%src%\tags.txt" . >nul:

echo %src%>.link
attrib -h .link

echo Les images de 'Graphic tools' ont été récupérées localement au projet.
echo Maintenant, vous pouvez modifier les copies locales.
echo Regardez le fichier README.md
